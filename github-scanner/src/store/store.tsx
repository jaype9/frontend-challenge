import { applyMiddleware, combineReducers, createStore, Store } from 'redux';
import thunk from 'redux-thunk';
import { Repositorios, repositoriosReducer } from '../reducers/repositoriosReducer';
import { PullRequests, pullRequestsReducer } from '../reducers/pullRequestsReducer';

// Cria interface para tipar a store da aplicação
export interface AplicativoState {
  repositoriosState: Repositorios;
  pullRequestsState: PullRequests;
}

const rootReducer = combineReducers<AplicativoState>({
  repositoriosState: repositoriosReducer,
  pullRequestsState: pullRequestsReducer,
});

export default function configuraStore(): Store<AplicativoState, any> {
  const store = createStore(rootReducer, undefined, applyMiddleware(thunk));
  return store;
}