import { Dono } from "./Dono";
import { Repositorio } from "./Repositorio";

interface Base {
  repo: Repositorio;
}

export interface PullRequest {
  user: Dono;
  body: string;
  title: string;
  created_at: string;
  base: Base;
  id: number;
  html_url: string;
}