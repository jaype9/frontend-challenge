export interface Dono {
  login: string;
  id: number;
  avatar_url: string;
}