import { Dono } from './Dono';

export interface Repositorio {
  id: number;
  name: string;
  owner: Dono;
  description: string;
  stargazers_count: number;
  forks_count: number;
}

