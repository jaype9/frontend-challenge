import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import ApiCalls from '../services/ApiCalls';
import { PullRequest } from '../models/PullRequests';
import { PullRequests } from '../reducers/pullRequestsReducer';
import { Repositorio } from '../models/Repositorio';

// Cria as constantes dos tipos de ação
export enum PullRequestsActionTypes {
  BUSCAPULLREQUESTS = 'BUSCAPULLREQUESTS',
  LOADINGPULLREQUESTS = 'LOADINGPULLREQUESTS',
  BUSCAREPOSITORIO = 'BUSCAREPOSITORIO',
}

// Cria interface que define parametros de cada ação
export interface BuscaPullRequestsAction {
  type: PullRequestsActionTypes.BUSCAPULLREQUESTS;
  pullRequests: PullRequest[];
}
export interface LoadingPullRequestsAction {
  type: PullRequestsActionTypes.LOADINGPULLREQUESTS;
}
export interface BuscaRepositorioAction {
  type: PullRequestsActionTypes.BUSCAREPOSITORIO;
  repositorio: Repositorio;
}

// Combina as ações do tipo consulta
export type PullRequestsActions = 
  LoadingPullRequestsAction |
  BuscaPullRequestsAction |
  BuscaRepositorioAction;


/* As ações devem seguir a tipagem:
<Promise<Tipo do retorno>, 
Tipo do reducer no qual ela está operando, 
Tipo do parametro que recebe,
Tipo da ação> */
export const buscaPullRequests: 
  ActionCreator<
    ThunkAction<Promise<any>, 
    PullRequests, 
    string, 
    PullRequestsActions
  >
> = (repo: string, criador: string) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: PullRequestsActionTypes.LOADINGPULLREQUESTS,
    });
    const resultado = await ApiCalls.buscaPullRequests(repo,criador);
    if(resultado.length===0) {
      const repositorio = await ApiCalls.buscaRepositorio(repo,criador);
      return dispatch({
        repositorio: repositorio,
        type: PullRequestsActionTypes.BUSCAREPOSITORIO,
      });
    }
    return dispatch({
      pullRequests: resultado,
      type: PullRequestsActionTypes.BUSCAPULLREQUESTS,
    });
  };
};