import React from 'react';
import TelaBuscaRepositorios from './views/TelaBuscaRepositorios/TelaBuscaRepositorios';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import configuraStore from './store/store';
import NavegadorSuperior from './views/TelaBuscaRepositorios/NavegadorSuperior';
import TelaPullRequests from './views/TelaPullRequests/TelaPullRequests';

function App() {

  const store = configuraStore();
  
  return (
    <Provider store={store}>
      <NavegadorSuperior cabecalho={'Busca Repositórios'} />
      <BrowserRouter>
        <Route exact path="/" component={TelaBuscaRepositorios} />
        <Route path="/:criador/:repo/pulls" component={TelaPullRequests} />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
