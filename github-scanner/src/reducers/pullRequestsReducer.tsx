import { Reducer } from 'redux';
import { PullRequest } from '../models/PullRequests';
import { PullRequestsActions, PullRequestsActionTypes } from '../actions/PullRequestsActions';
import { Repositorio } from '../models/Repositorio';

export interface PullRequests {
  readonly pullRequests: PullRequest[];
  readonly loading: boolean;
  readonly repositorio: Repositorio;
}

const estadoInicialPullRequests: PullRequests = {
  pullRequests: [],
  repositorio: {
    id: 0,
    name: '',
    owner: {
      login: '',
      id: 0,
      avatar_url: '',
    },
    description: '',
    stargazers_count: 0,
    forks_count: 0,
  },
  loading: false,
};

export const pullRequestsReducer: Reducer<PullRequests, PullRequestsActions> = (
  state = estadoInicialPullRequests,
  action
) => {
  switch (action.type) {
    case PullRequestsActionTypes.BUSCAPULLREQUESTS: {
      return {
        ...state,
        pullRequests: action.pullRequests,
        repositorio: action.pullRequests[0].base.repo,
        loading: false,
      };
    }
    case PullRequestsActionTypes.LOADINGPULLREQUESTS: {
      return {
        ...state,
        loading: true,
      };
    }
    case PullRequestsActionTypes.BUSCAREPOSITORIO: {
      return {
        ...state,
        repositorio: action.repositorio,
        pullRequests: [],
        loading: false,
      };
    }
    default:
      return state;
  }
};
