import React, { useState, useEffect } from 'react';
import { Grid, Typography, CircularProgress, useMediaQuery, useTheme } from '@material-ui/core';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { useParams } from 'react-router-dom';
import RepositorioItem from '../TelaBuscaRepositorios/RepositorioItem';
import PullRequestItem from './PullRequestItem';
import { AplicativoState } from '../../store/store';
import { PullRequest } from '../../models/PullRequests';
import { buscaPullRequests } from '../../actions/PullRequestsActions';
import { PullRequests } from '../../reducers/pullRequestsReducer';
import { Repositorio } from '../../models/Repositorio';
import { useWindowHeight } from '@react-hook/window-size';

type DispatchProps = {
  buscaPullRequests: (repo: string, criador: string) => void;
};

type StateProps = {
  pullRequests: PullRequest[];
  loading: boolean;
  repositorio: Repositorio;
};

type Props = StateProps & DispatchProps;

type RouteParams = {
  repo: string,
  criador: string
}

const TelaPullRequests: React.FC<Props> = props => {
  const windowHeight = useWindowHeight();
  const theme = useTheme();

  const toolBarHeight = useMediaQuery(theme.breakpoints.up('sm')) ? 64 : 56;

  const alturaMinLateral = windowHeight - toolBarHeight;

  const [loadInicial,setLoadInicial] = useState(true);

  let params = useParams<RouteParams>();

  useEffect(() => {
    props.buscaPullRequests(params.repo,params.criador);
  },[])

  useEffect(() => {
    if(!props.loading && 
      (props.pullRequests.length > 0 || props.repositorio.id !== 0)
    ) {
      setLoadInicial(false);
    }
  },[props.loading])

  return (
    <Grid container item justify='center' >
      {
        (loadInicial || props.loading) ? 
        <Grid container item justify='center' alignItems='center' style={{height:'90vh'}}>
          <Grid container item justify='center' >
            <CircularProgress variant="indeterminate" /> 
          </Grid>
        </Grid>
        :
        <Grid container item justify='center' style={{backgroundColor:'#f7f7f7',minHeight:alturaMinLateral}} >
          <Grid xs={12} md={6} container item alignItems='center' 
            style={{backgroundColor:'white'}} direction='column'
          >
            <Grid container item>
              <RepositorioItem repositorio={props.repositorio} chevron={false} 
                divider={false} 
              />
            </Grid>
            <Typography variant='h6'>
              Pull Requests
            </Typography>
            <Grid container item >
              {props.pullRequests.map( pullRequest => (
                <PullRequestItem key={pullRequest.id} pullRequest={pullRequest} />
              ))}
            </Grid>
          </Grid>
        </Grid>
      }
    </Grid>
  );
}

const mapStateToProps = (store: AplicativoState) => {
  return {
    pullRequests: store.pullRequestsState.pullRequests,
    loading: store.pullRequestsState.loading,
    repositorio: store.pullRequestsState.repositorio,
  };
};

const mapDispatchToProps = (
  dispatch: ThunkDispatch<
    PullRequests, 
    any,
    AnyAction
  >
): DispatchProps => ({
  buscaPullRequests: (repo: string, criador: string) => dispatch(buscaPullRequests(repo, criador)),
});

export default connect(mapStateToProps,mapDispatchToProps)(TelaPullRequests);
