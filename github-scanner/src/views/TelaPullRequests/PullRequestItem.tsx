import React from 'react';
import { Grid, Typography, ListItem, ListItemText, createStyles, makeStyles, Avatar, ListItemAvatar } from '@material-ui/core';
import Octicon from 'octicons-react-ts-color';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { PullRequest } from '../../models/PullRequests';

const useStyles = makeStyles((theme) =>
  createStyles({
    loading: {
      display:'flex',
      justifyContent:'center',
      marginTop:10,
    },
    data: {
      position: 'absolute',
      right: 16,
      top: 10,
    },
    chevron: {
      position: 'absolute',
      right: 7,
      bottom: '30%',
    },
  }),
);

interface Props {
  pullRequest: PullRequest;
}

const RepositorioItem: React.FC<Props> = ({pullRequest}) => {
  const clases = useStyles();

  const titulo = () => (
    <Typography
      component="span"
      variant="h6"
      color="textPrimary"
    >
      {pullRequest.title}
    </Typography>
  );

  const corpo = () => (
    <div style={{overflow:'hidden',textOverflow:'ellipsis',lineHeight: '1.2em',maxHeight: '3.6em'}}>
      <Typography
        component='span'
        variant='body2'
        color='textSecondary'
        style={{marginLeft:7}}
      >
        {pullRequest.body}
      </Typography>
    </div>
  );

  const formataData = (data: string): string => {
    const dataObj = new Date(data);
    return dataObj.getDate() + '/' +
      (dataObj.getMonth() + 1) + '/' +
      dataObj.getFullYear();
  }

  const abrirUrl = () => {
    window.open(pullRequest.html_url, "_blank");
  }

  return (
    <Grid container item>
      <ListItem divider alignItems='flex-start' onClick={abrirUrl}
        style={{paddingRight:30,paddingTop:20}} 
      >
        <ListItemAvatar >
          <div style={{width:50}} >
            <Avatar style={{marginLeft:5}} src={pullRequest.user.avatar_url} />
            <Typography align='center' style={{fontSize:'0.75rem',overflowWrap:'break-word'}} >
              {pullRequest.user.login}
            </Typography>
          </div>
        </ListItemAvatar>
        <ListItemText disableTypography primary={titulo()} secondary={corpo()} />
        <div className={clases.data}>
          <Typography style={{fontSize:'0.70rem',marginLeft:5}} color="textSecondary">
            {formataData(pullRequest.created_at)}
          </Typography>
        </div>
        <div className={clases.chevron}>
          <ChevronRightIcon style={{color:'rgba(0, 0, 0, 0.54)'}} />
        </div>
      </ListItem>
    </Grid>
  );
}

export default RepositorioItem;
