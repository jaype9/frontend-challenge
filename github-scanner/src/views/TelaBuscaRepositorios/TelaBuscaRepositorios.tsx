import React, { useState } from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { Grid } from '@material-ui/core';
import Buscador from './Buscador';
import ConfiguraFiltros from './ConfiguraFiltros';
import ListaRepositorios from './ListaRepositorios';
import { Repositorio } from '../../models/Repositorio';
import { AplicativoState } from '../../store/store';
import { Repositorios } from '../../reducers/repositoriosReducer';
import { buscaRepositorios, proximaPagina, preparaParaCarregar } from '../../actions/RepositoriosActions';
import ListaVazia from './ListaVazia';

export type Filtro = {
  nome: string,
  ascendente: boolean,
}

type DispatchProps = {
  buscaRepositorios: (nomeBuscado: string, filtro: Filtro) => void;
  proximaPagina: (nomeBuscado: string, filtro: Filtro, paginaAtual: number) => void;
  esvazia: () => void;
};

type StateProps = {
  repositorios: Repositorio[];
  haMaisPaginas: boolean;
  paginaAtual: number;
};

type Props = StateProps & DispatchProps;

const TelaBuscaRepositorios: React.FC<Props> = props => {

  const [buscador, setBuscador] = React.useState<null | HTMLElement>(null);

  const [nomeRepositorio,setNomeRepo] = useState('');

  const [modal,setModal] = useState(false);

  const [filtro,setFiltro] = useState({nome: '', ascendente: false});

  const fechaModal = () => {
    props.esvazia();
    props.buscaRepositorios(nomeRepositorio,filtro)
    setModal(false);
  }

  const toggleNoModal = (event: React.MouseEvent<HTMLElement>) => {
    setBuscador(event.currentTarget);
    setModal(!modal);
  };

  const buscaMais = (): any => {
    props.proximaPagina(nomeRepositorio,filtro,props.paginaAtual);
  }

  const atualizaLista = (): void => {
    props.esvazia();
    props.buscaRepositorios(nomeRepositorio,filtro);
  }

  const temMais = (): boolean => {
    return props.haMaisPaginas;
  }

  return (
    <Grid container item justify='center' style={{backgroundColor:'#f7f7f7'}} >
      <Grid xs={12} md={6} container item justify='center' 
        style={{backgroundColor:'white'}} 
      >
        <Buscador setNomeRepo={setNomeRepo} filtro={filtro}
          abrirEditor={toggleNoModal} fecharEditor={fechaModal} 
        />
        <ConfiguraFiltros setFiltro={setFiltro} modal={modal} 
          anchorEl={buscador} fechaModal={fechaModal}
        />
        <ListaRepositorios repositorios={props.repositorios} 
          buscaMais={buscaMais} temMais={temMais} atualizaLista={atualizaLista} 
        />
        <ListaVazia qtdItems={props.repositorios.length} 
          paginaAtual={props.paginaAtual} campoBusca={nomeRepositorio} />
      </Grid>
    </Grid>
  );
}

const mapStateToProps = (store: AplicativoState) => {
  return {
    repositorios: store.repositoriosState.repositorios,
    paginaAtual: store.repositoriosState.paginaAtual,
    haMaisPaginas: store.repositoriosState.haMaisPaginas,
  };
};

const mapDispatchToProps = (
  dispatch: ThunkDispatch<
    Repositorios, 
    any,
    AnyAction
  >
): DispatchProps => ({
  buscaRepositorios: (nomeBuscado: string, filtro: Filtro) => dispatch(buscaRepositorios(nomeBuscado, filtro)),
  proximaPagina: (nomeBuscado: string, filtro: Filtro, paginaAtual: number) => dispatch(proximaPagina(nomeBuscado, filtro, paginaAtual)),
  esvazia: () => dispatch(preparaParaCarregar()),
});

export default connect(mapStateToProps,mapDispatchToProps)(TelaBuscaRepositorios);
