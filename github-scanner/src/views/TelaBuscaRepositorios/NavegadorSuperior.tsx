import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import GitHubIcon from '@material-ui/icons/GitHub';

type Props = {
  cabecalho: string;
}

const NavegadorSuperior: React.FC<Props> = ({cabecalho}) => {
  return (
    <div>
      <AppBar position="static" style={{backgroundColor:'#24292e'}}>
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <GitHubIcon />
          </IconButton>
          <Typography variant="h6" >
            {cabecalho}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default NavegadorSuperior;
