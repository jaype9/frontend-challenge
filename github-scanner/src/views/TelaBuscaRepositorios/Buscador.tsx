import React, { useRef } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import { Button, Grid } from '@material-ui/core';
import { Filtro } from './TelaBuscaRepositorios';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center'
    },
    input: {
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    espacamentoNavegador: {
      marginTop:10,
    },
  }),
);

type Props = {
  setNomeRepo: Function;
  filtro: Filtro;
  abrirEditor: Function;
  fecharEditor: Function;
}

export const heightBuscador = 58;

const Buscador: React.FC<Props> = ({setNomeRepo,filtro, abrirEditor, fecharEditor}) => {
  const clases = useStyles();
  const input = useRef<HTMLInputElement>(null);

  let IconeAscendente = KeyboardArrowDownIcon;
  if(!filtro.ascendente) {
    IconeAscendente = KeyboardArrowUpIcon;
  }
  
  const concluirEdicao = (event: React.FormEvent<HTMLInputElement>) => {
    event.preventDefault();
    fecharEditor();
  }
  
  return (
    <Grid container item justify='center' className={clases.espacamentoNavegador} >
      <div style={{width:'90%'}} onClick={(e) => abrirEditor(e)}>
        <Paper component="form" className={clases.root}
          onSubmit={concluirEdicao}>
          <IconButton className={clases.iconButton}>
            <SearchIcon />
          </IconButton>
          <InputBase
            ref={input}
            className={clases.input}
            placeholder="Nome do repositório"
            onChange={(e) => setNomeRepo(e.target.value)}
          />
          {
            (filtro.nome !== '') ? 
            <Button variant="outlined" size='small' style={{paddingLeft:17}}>
              {filtro.nome}
              <IconeAscendente fontSize='small' style={{position:'absolute',left:0}} />
            </Button> : null
          }
        </Paper>
      </div>
    </Grid>
  );
}

export default Buscador;
