import React from 'react';
import { Typography } from '@material-ui/core';

interface Props {
  qtdItems: number;
  campoBusca: string;
  paginaAtual: number;
}

const ListaVazia: React.FC<Props> = ({qtdItems,campoBusca,paginaAtual}) => {
  return (
    <div style={{position:'absolute',top:'50%',width:'90%',display:'flex',justifyContent:'center'}} >
        {qtdItems === 0 && paginaAtual === 1 ? 
          <Typography color='textSecondary' style={{overflowWrap:'break-word'}} >
            Não há resultados para '{campoBusca}'.
          </Typography> : null
        }
    </div>
  );
}

export default ListaVazia;
