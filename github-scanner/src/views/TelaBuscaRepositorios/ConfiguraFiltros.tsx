import React, { useState } from 'react';
import { Popper, Paper, ClickAwayListener, Grid, Typography } from '@material-ui/core';
import EscorregaCima from './EscorregaCima';
import BotaoFiltro from './BotaoFiltro';

type Props = {
  modal: boolean;
  anchorEl: null | HTMLElement;
  setFiltro: Function;
  fechaModal: Function;
}

const ConfiguraFiltros: React.FC<Props> = ({modal,anchorEl,setFiltro,fechaModal}) => {

  const [stars,setStars] = useState(0);
  const [forks,setForks] = useState(0);
  const [issues,setIssues] = useState(0);
  const [updates,setUpdates] = useState(0);

  const arraySetters = [setStars,setForks,setIssues,setUpdates];

  const updateFiltro = (nome: String) => {
    let indice = 0;
    botoes.forEach((botao,i) => {
      (botao.nome===nome) ?
        indice = i : arraySetters[i](0)
    });
    let incremento = (botoes[indice].status+1)%3;
    arraySetters[indice](incremento);
    switch(incremento) {
      case 1:
        setFiltro({nome: botoes[indice].nome,ascendente: false})
        break;
      case 2:
        setFiltro({nome: botoes[indice].nome,ascendente: true})
        break;
      default:
        setFiltro({nome: '',ascendente: true})
        break;
    }
  }

  let botoes = [
    {
      nome: 'stars',
      icon:'star',
      status: stars,
    },
    {
      nome: 'forks',
      icon:'git-branch',
      status: forks,
    },
    {
      nome: 'issues',
      icon:'issue-opened',
      status: issues,
    },
    {
      nome: 'updates',
      icon:'update',
      status: updates,
    },
  ];

  return (
    <Popper open={modal} anchorEl={anchorEl} placement="top" transition style={{marginTop:5}} >
    {({ TransitionProps }) => (
      <EscorregaCima {...TransitionProps}>
        <ClickAwayListener onClickAway={(e) => {e.preventDefault(); fechaModal();}}>
          <Paper style={{padding:20,width:'300px'}}>
            <Grid container direction='column' alignItems='center' spacing={2} >
              <Typography>
                você pode ordenar por:
              </Typography>
              <Grid container direction='row' justify='space-between' >
                {botoes.map((botao,indice) => 
                  <BotaoFiltro key={indice} {...botao} updateFiltro={updateFiltro} />
                )}
              </Grid>
            </Grid>
          </Paper>
        </ClickAwayListener>
      </EscorregaCima>
    )}
    </Popper>
  );
}

export default ConfiguraFiltros;
