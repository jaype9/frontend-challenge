import React from 'react';
import { CircularProgress, createStyles, makeStyles, useTheme, useMediaQuery, Grid } from '@material-ui/core';
import InfiniteScroll from "react-infinite-scroll-component";
import { Repositorio } from '../../models/Repositorio';
import { useWindowHeight } from '@react-hook/window-size';
import { heightBuscador } from './Buscador';
import RepositorioItem from './RepositorioItem';

const useStyles = makeStyles((theme) =>
  createStyles({
    loading: {
      marginTop:10,
    },
  }),
);


interface Props {
  repositorios: Repositorio[];
  buscaMais: () => any;
  temMais: () => boolean;
  atualizaLista: () => void;
}

const ListaRepositorios: React.FC<Props> = ({repositorios,buscaMais,temMais,atualizaLista}) => {
  const clases = useStyles();
  const windowHeight = useWindowHeight();
  const theme = useTheme();

  const toolBarHeight = useMediaQuery(theme.breakpoints.up('sm')) ? 64 : 56;

  const alturaIdealScroll = windowHeight - heightBuscador - toolBarHeight;

  return (
    <Grid container item justify='center' >
      <InfiniteScroll
        pullDownToRefresh
        pullDownToRefreshContent={
          <Grid container item justify='center' className={clases.loading}>
            <CircularProgress variant="static" value={25} /> 
          </Grid>
        }
        releaseToRefreshContent={
          <Grid container item justify='center' className={clases.loading}> 
            <CircularProgress variant="static" value={50} /> 
          </Grid>
        }
        refreshFunction={atualizaLista}
        dataLength={repositorios.length}
        next={buscaMais}
        hasMore={temMais()}
        loader={ <Grid container item justify='center' className={clases.loading}> <CircularProgress /> </Grid>}
        height={alturaIdealScroll}
      >
        {repositorios.map((repositorio) => (
          <RepositorioItem key={repositorio.id} repositorio={repositorio} />
        ))}
      </InfiniteScroll>
    </Grid>
  );
}

export default ListaRepositorios;
