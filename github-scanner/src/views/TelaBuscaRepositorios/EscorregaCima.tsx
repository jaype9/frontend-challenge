import React from 'react';
import {useSpring, animated} from 'react-spring';

type EscorregaProps = {
  children?: React.ReactElement;
  in?: boolean;
  onEnter?: () => {};
  onExited?: () => {};
}

const EscorregaCima = React.forwardRef<HTMLDivElement, EscorregaProps>(function EscorregaCima(props, ref) {
  const { in: open, children, onEnter, onExited, ...other } = props;
  const style = useSpring({
    from: { opacity: 0, marginTop: +500 },
    to: { opacity: open ? 1 : 0, marginTop: open? 0 : +500 },
    onStart: () => {
      if (open && onEnter) {
        onEnter();
      }
    },
    onRest: () => {
      if (!open && onExited) {
        onExited();
      }
    },
  });

  return (
    <animated.div ref={ref} style={style} {...other}>
      {children}
    </animated.div>
  );
});

export default EscorregaCima;