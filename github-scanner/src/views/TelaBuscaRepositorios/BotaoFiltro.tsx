import React from 'react';
import { IconButton, Grid, Typography, useTheme } from '@material-ui/core';
import UpdateIcon from '@material-ui/icons/Update';
import Octicon from 'octicons-react-ts-color';

type Props = {
  nome: string;
  icon: any;
  status: number;
  updateFiltro: Function;
}

const BotaoFiltro: React.FC<Props> = ({nome, icon, status,updateFiltro}) => {
  const theme = useTheme();
  let update = nome==='updates';

  const getColor = (): 'primary' | 'secondary' | undefined => {
    switch(status) {
      case 0:
        return undefined;
      case 1:
        return 'primary';
      case 2:
        return 'secondary';
    }
    return undefined;
  }

  const getColorIcon = (): string => {
    switch(status) {
      case 0:
        return 'black';
      case 1:
        return theme.palette.primary.main;
      case 2:
        return theme.palette.secondary.main;
    }
    return 'black';
  }

  return (
    <Grid container item direction='column' alignItems='center'  xs={3} >
      <IconButton style={update?{marginTop:-3}:{}}
        color={getColor()} onClick={() => updateFiltro(nome)} >
        {(update) ?
          <UpdateIcon fontSize='large' style={{color:getColorIcon()}} />
        :
          <Octicon name={icon} ratio={2} color={getColorIcon()} />
        }
      </IconButton>
      <Typography variant='subtitle2' color={getColor()} style={{marginTop:-15}}>
        {nome}
      </Typography>
    </Grid>
  );
}

export default BotaoFiltro;
