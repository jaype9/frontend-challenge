import React from 'react';
import { Grid, Typography, ListItem, ListItemText, createStyles, makeStyles, Avatar, ListItemAvatar } from '@material-ui/core';
import Octicon from 'octicons-react-ts-color';
import { Repositorio } from '../../models/Repositorio';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) =>
  createStyles({
    loading: {
      display:'flex',
      justifyContent:'center',
      marginTop:10,
    },
    branchEStars: {
      position: 'absolute',
      right: 16,
      top: '50%',
      transform: 'translateY(-50%)',
    },
    chevron: {
      position: 'absolute',
      right: 7,
      bottom: -3,
    },
    ajusteIconeFork: {
      marginBottom: -3,
    },
    ajusteIconeStar: {
      marginBottom: -8,
      marginLeft:-5,
    }
  }),
);

interface Props {
  repositorio: Repositorio;
  chevron?: boolean;
  divider?: boolean;
}

const RepositorioItem: React.FC<Props> = ({
  repositorio, chevron = true, divider = true
}) => {
  const clases = useStyles();
  const history = useHistory();

  const titulo = () => (
    <React.Fragment>
      <Typography
        component="span"
        variant="h4"
        style={{display:'inline'}}
        color="textPrimary"
      >
        /
      </Typography>
      <Typography
        component="span"
        variant="h6"
        style={{display:'inline'}}
        color="textPrimary"
      >
        {' '+repositorio.name}
      </Typography>
    </React.Fragment>
  );

  const abrePullReq = () => {
    history.push(`/${repositorio.owner.login}/${repositorio.name}/pulls`)
  }

  return (
    <Grid container item>
      <ListItem divider={divider} alignItems='flex-start' onClick={abrePullReq}
        style={{paddingRight:75,paddingBottom:10}} >
        <ListItemAvatar >
          <div style={{width:50}} >
            <Avatar style={{marginLeft:5}} src={repositorio.owner.avatar_url} />
            <Typography align='center' style={{fontSize:'0.75rem',overflowWrap:'break-word'}} >
              {repositorio.owner.login}
            </Typography>
          </div>
        </ListItemAvatar>
        <ListItemText primary={titulo()} secondary={repositorio.description}
          secondaryTypographyProps={{style:{marginLeft:7}}} 
          />
        <div className={clases.branchEStars}>
          <Grid container direction='column' alignItems='flex-start' spacing={1}>
            <Grid item container>
              <Octicon name='git-branch' color={'rgba(0, 0, 0, 0.54)'} class={[clases.ajusteIconeFork]} />
              <Typography style={{fontSize:'0.70rem',marginLeft:5}} color="textSecondary" display='inline' >
                {repositorio.forks_count}
              </Typography>
            </Grid>
            <Grid item container>
              <Octicon name='star' color={'rgba(0, 0, 0, 0.54)'} class={[clases.ajusteIconeStar]} />
              <Typography style={{fontSize:'0.70rem',marginLeft:5}} color="textSecondary" display='inline' >
                {repositorio.stargazers_count}
              </Typography>
            </Grid>
          </Grid>
        </div>
        { chevron ?
          <div className={clases.chevron} >
            <ChevronRightIcon style={{color:'rgba(0, 0, 0, 0.54)'}} />
          </div> : null
        }
      </ListItem>
    </Grid>
  );
}

export default RepositorioItem;
