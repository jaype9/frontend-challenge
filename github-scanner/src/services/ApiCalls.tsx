import { Filtro } from "../views/TelaBuscaRepositorios/TelaBuscaRepositorios";


export default class ApiCalls {

  private static readonly url = 'https://api.github.com/';

  static buscaRepositorios(
    nomeBuscado: string, 
    filtro: Filtro, 
    numeroPagina: number
  ): Promise<any> {
    
    let urlEspecifica = 'search/repositories?q=';
    // seta filtro de texto
    if(nomeBuscado !== '') {
      urlEspecifica = urlEspecifica.concat(nomeBuscado);
      urlEspecifica = urlEspecifica.concat('+');
    }
    //seta linguagem javascript
    urlEspecifica = urlEspecifica.concat('language:Javascript');
    //seta tipo de ordenacao
    if(filtro.nome !== '') {
      urlEspecifica = urlEspecifica.concat('&sort=');
      urlEspecifica = urlEspecifica.concat(ApiCalls.deParaFiltros(filtro.nome));
      if(filtro.ascendente) {
        urlEspecifica = urlEspecifica.concat('&order=asc');
      }
    }
    // seta a pagina
    urlEspecifica = urlEspecifica.concat('&page=');
    urlEspecifica = urlEspecifica.concat(numeroPagina+'');
    
    return fetch(ApiCalls.url+urlEspecifica)
      .then(ApiCalls.lidaComErros)
      .then(response => response.json())
      .catch(error => console.log(error));
  }

  static buscaRepositorio(
    repositorio: string, 
    donoRepositorio: string
  ): Promise<any> {
    
    let urlEspecifica = 'repos/';
    
    urlEspecifica = urlEspecifica.concat(donoRepositorio+'/');
    urlEspecifica = urlEspecifica.concat(repositorio);
    
    return fetch(ApiCalls.url+urlEspecifica)
      .then(ApiCalls.lidaComErros)
      .then(response => response.json())
      .catch(error => console.log(error));
  }

  static buscaPullRequests(
    repositorio: string, 
    donoRepositorio: string
  ): Promise<any> {
    
    let urlEspecifica = 'repos/';
    
    urlEspecifica = urlEspecifica.concat(donoRepositorio+'/');
    urlEspecifica = urlEspecifica.concat(repositorio+'/');
    urlEspecifica = urlEspecifica.concat('pulls');
    
    return fetch(ApiCalls.url+urlEspecifica)
      .then(ApiCalls.lidaComErros)
      .then(response => response.json())
      .catch(error => console.log(error));
  }

  private static lidaComErros(response: Response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }

  private static deParaFiltros(nomeFiltro: string): string {
    switch(nomeFiltro) {
      case 'issues':
        return 'help-wanted-issues';
      case 'updates':
        return 'updated';
      default:
        return nomeFiltro;
    }
  }

}

